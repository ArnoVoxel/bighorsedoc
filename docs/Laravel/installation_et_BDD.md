# Installer Laravel et Préparer la BDD

# Préparation de la machine et de la BDD


L'environnement de production reposant sur Laravel 8.0 pour cohabiter avec Vue.JS 2.

Il faut donc installer php version 7.4.30 : 
```cli
brew install php@7.4
```

Et l'autre élément indispensable est l'installation de composer : 
```cli
brew install composer
```

On peut alors créer un projet en Laravel 8 : 

```cli
composer create-project --prefer-dist laravel/laravel="8.0 nomDuProjet"
```

# Timezone

Pour travailler sur la bonne timezone, on peut modifier le fichier .env en ajoutant cette ligne :

```
APP_TIMEZONE='Europe/Paris'
```
> se référer à la doc de PHP si besoin pour les timezone supportées.

Puis faire référence à cette variable dans le fichier app.php situé dans le dossier **config** :
```
'timezone' => env('APP_TIMEZONE', 'UTC'),
```
> le second paramètre ('UTC') est une valeur par défaut.

---


## SYNTHAXE

Method **avec parenthèses** appelle en requête (querybuilder) et pouvoir utiliser les filtres SQL (where order …)
__Exemple__ 
```php
suivis()->where(‘utilisateur_id’, $auth()->user()->id
```

Method **sans parenthèse** appelle le tableau (attribut) (le résultat d’une requête)

---
# Créer une entité / Migration

On crée le fichier de migration :
```cli
php artisan make:migration nom_de_ma_migration
```


Celà crée une class avec deux fonctions, **up** et **down**

Dans la fonction **up** on ajoute les colonnes et leur type:
```php
    public function up()
    {
        Schema::create('utilisateurs', function (Blueprint $table) {
            $table->id();
            $table->string('email')->unique();
            $table->string('password');
            $table->timestamps();
        });
    }
```

Ensuite on lance la migration :
```cli
php artisan migrate
```


---
# Factory / FAKER

Pour remplir nos tables, nous allons devoir générer des données avec le système de **factory** de Laravel.

## Le Model

En premier lieu il faut créer le model

```cli
php artisan make:model MonNom
```

On fait appel à la méthode **HasFactory**, puis on renseigne les champs modifiables de la BDD:
```php
use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'email',
    ];
```

>Si on ne veut pas de timestamps il faut le préciser dans le modèle en ajoutant ```public $timestamps = false;```

## Générer des fausses données

On crée ensuite un fichier pour générer des fausses données afin de travailler sur le fonctionnement de l'application en environnement de développement :

```cli
php artisan make:factory MonNomFactory
```

On se rend alors dans le dossier database qui contient un sous-dossier factories.

Il faut alors préciser ce qu'on veut créer comme fausses données.
On peut utiliser faker pour créer de fausses données :
```php
public function definition()
    {
        return [
		'nom' => $this->faker->name,
		'email' => $this->faker->unique()->safeEmail,
        ];
    }
```

Ensuite on va remplir la BDD avec ces données, c'est le rôle du **seeder** :
```cli
php artisan make:seed MonNomTableSeeder
```

Dans ce fichier, on pourra alors dire combien d'éléments doivent être créés en BDD :

```php
public function run()
    {
        Utilisateur::factory()->count(5)->create();
    }
```

Ne reste plus qu'à lancer tout ça : 

```cli
php artisan db:seed --class=MonNomTableSeeder
```
Tant que l'on reste en environnement de développement, on peut se permettre de supprimer les tables de la BDD pour les reconstruire et les peupler. Par définition, en environnement de développement, il peut y avoir des essais et des modifications qui pourraient engendrer des erreurs. On peut alors dès que l'ensemble des fichiers de migration, les model, les factory et les seeder sont corrects, relancer la création proprement :

Il faut au préalable intégrér les seeder dans le fichier DatabaseSeeder.php : 

```php
/**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MonPremierTableSeeder::class);
        $this->call(UnAutreTableSeeder::class);
        $this->call(UnTroisiemeTableSeeder::class);
    }
```

Alors on peut lancer le tout :

```cli
php artisan migrate:fresh --seed
```


Il faut alors ajouter les relations dans nos models :

https://laravel.com/docs/9.x/eloquent-relationships#has-one-of-many

